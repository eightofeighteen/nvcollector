﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nvCollector
{
    class NVBackupJob
    {
        public string ID
        {
            get;
            set;
        }

        public string Title
        {
            get;
            set;
        }

        public string Type
        {
            get;
            set;
        }

        public string Plugin
        {
            get;
            set;
        }

        public string Client
        {
            get;
            set;
        }

        public string Selection
        {
            get;
            set;
        }

        public string Schedule
        {
            get;
            set;
        }

        public string Target
        {
            get;
            set;
        }

        public string AdvancedOptions
        {
            get;
            set;
        }

        public string RunStatus
        {
            get;
            set;
        }

        public string NextRunTime
        {
            get;
            set;
        }

        public override string ToString()
        {
            string result = ID + "\t" + Title + "\t" + Type + "\t" + Plugin + "\t" + Client + "\t" + Selection + "\t" + Schedule + "\t" + Target + "\t" + AdvancedOptions + "\t" + RunStatus + "\t" + NextRunTime;
            return result;
        }

        public void ReadLine(string line, char delim)
        {
            List<string> elements = line.Split(delim).ToList<string>();
            ID = elements[0];
            Title = elements[1];
            Type = elements[2];
            Plugin = elements[3];
            Client = elements[4];
            Selection = elements[5];
            Schedule = elements[6];
            Target = elements[7];
            AdvancedOptions = elements[8];
            RunStatus = elements[9];
            NextRunTime = elements[10];
        }

    }

}
