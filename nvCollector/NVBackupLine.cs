﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.Data.Linq;
using System.IO;

namespace nvCollector
{
    class NVBackupLine
    {
        public string ID
        {
            get;
            set;
        }

        public string Client
        {
            get;
            set;
        }

        public string Title
        {
            get;
            set;
        }

        public string Status
        {
            get;
            set;
        }

        public string Log
        {
            get;
            set;
        }

        public DateTime Timestamp
        {
            get;
            set;
        }

        public NVBackupLine()
        {

        }

        private static bool ListContains(List<string> list, string target, bool lastLine = false)
        {
            if (lastLine)
            {
                if (list[list.Count - 1].Contains(target))
                    return true;
            }
            else
            {

                foreach (string line in list)
                {
                    if (line.Contains(target))
                        return true;
                }
            }
            return false;
        }

        private static void StripBlanks(List<string> list)
        {
            //bool data = false;

            while (list[list.Count - 1].Length == 0)
            {
                list.RemoveAt(list.Count - 1);
            }
        }

        public NVBackupLine(NVBackupJob Job, NVBackupLog Log)
        {
            ID = Job.ID;
            Title = Job.Title;
            Client = Job.Client;
            //string lastResult = Log.Log.Split('\n').ToList<string>()[Log.Log.Split('\n').ToList<string>().Count - 3];
            List<string> lastResult = Log.Log.Split('\n').ToList<string>();
            StripBlanks(lastResult);
            //Status = lastResult;

            if (ListContains(lastResult, "Backup Aborted"))
                Status = "Job aborted";
            else if (ListContains(lastResult, "No suitable media for job", true))
                Status = "Waiting for media";
            else if (ListContains(lastResult, "abandoning job", true))
                Status = "Job failed";
            else if (ListContains(lastResult, "Finished job"))
                Status = "Successful";
            else
                Status = "In Progress/Other";
            this.Log = Log.Log;
            Timestamp = DateTime.Now;
        }

        public override string ToString()
        {
            return Client + "\t" + ID + "\t" + Title + "\t" + Timestamp + "\t" + Status;
        }

        public XElement generateXElement()
        {


            XElement result = new XElement("NVBackupLine",
                new XElement("Client", Client),
                new XElement("ID", ID),
                new XElement("Title", Title),
                new XElement("Status", Status),
                new XElement("Timestamp", Timestamp),
                new XElement("Log", Log));

            /*XElement result = new XElement("NData",
            new XElement("ComputerName", NisabaCore.ComputerName()),
            new XElement("DomainName", NisabaCore.DomainName()),
            new XElement("FullComputerName", NisabaCore.FullComputerName()),
            new XElement("Polled", DateTime.Now),
            new XElement("HwManufacturer", NisabaCore.HwManufacturer()),
            new XElement("HwModel", NisabaCore.HwModel()),
            new XElement("HwSerialNumber", NisabaCore.HwSerialNumber()),
            new XElement("ComputerUUID", NisabaCore.ComputerUUID()),
            new XElement("DeterminedAssetTag", NisabaCore.DeterminedAssetTag()),
            new XElement("ComputerDistinguishedName", NisabaCore.ComputerDistinguishedName()));

            XElement users = new XElement("Users");

            foreach (UserPair user in UserList)
            {
                users.Add(new XElement("UserPair",
                    new XElement("Username", user.Username),
                    new XElement("LastModified", user.LastModified),
                    new XElement("Active", user.Active)));
            }
            result.Add(users);*/

            return result;
        }

        public string generateXml()
        {
            return generateXElement().ToString();
        }

        public static string Delimit(string info, string delim)
        {
            string result = "";
            int b = info.IndexOf(delim) + delim.Length;
            int e = info.IndexOf("/" + delim);
            result = info.Substring(b + 1, e - b - 2);
            return result;
        }

        public static NVBackupLine readXml(string xml)
        {
            NVBackupLine j = new NVBackupLine();
            j.ID = Delimit(xml, "ID");
            j.Client = Delimit(xml, "Client");
            j.Title = Delimit(xml, "Title");
            j.Status = Delimit(xml, "Status");
            j.Timestamp = Convert.ToDateTime(Delimit(xml, "Timestamp"));
            j.Log = Delimit(xml, "Log");
            return j;
        }

    }
}
