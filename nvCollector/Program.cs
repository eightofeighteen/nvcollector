﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.Data.Linq;
using System.Net;
using System.Net.Http;
using System.Collections.Specialized;

namespace nvCollector
{


    class Program
    {

        public Object CreateObject(string XMLString, Object YourClassObject)
        {
            XmlSerializer oXmlSerializer = new XmlSerializer(YourClassObject.GetType());
            //The StringReader will be the stream holder for the existing XML file 
            YourClassObject = oXmlSerializer.Deserialize(new StringReader(XMLString));
            //initially deserialized, the data is represented by an object without a defined type 
            return YourClassObject;
        }

        /// <summary> 
        /// Converts a single XML tree to the type of T 
        /// </summary> 
        /// <typeparam name="T">Type to return</typeparam> 
        /// <param name="xml">XML string to convert</param> 
        /// <returns></returns> 
        public T XmlToObject<T>(string xml)
        {
            //using (var xmlStream = new StringReader(xml))
            //{
            var serializer = new XmlSerializer(typeof(T));
            //return (T)serializer.Deserialize(XmlReader.Create(xmlStream));
            StringReader rdr = new StringReader(xml);
            T resultingMessage = (T)serializer.Deserialize(rdr);
            //Console.WriteLine(resultingMessage);
            //Console.WriteLine("Trying to process:\n{0}", xml);
            return resultingMessage;

            /*XmlSerializer serializer = new XmlSerializer(typeof(NData));
             StringReader rdr = new StringReader(stuff);
             NData resultingMessage = (NData)serializer.Deserialize(rdr);
             * */
            //}
        }

        /// <summary> 
        /// Converts the XML to a list of T
        /// </summary> 
        /// <typeparam name="T">Type to return</typeparam> 
        /// <param name="xml">XML string to convert</param> 
        /// <param name="nodePath">XML Node path to select <example>//People/Person</example></param> 
        /// <returns></returns> 
        public List<T> XmlToObjectList<T>(string xml, string nodePath)
        {
            var xmlDocument = new XmlDocument();
            xmlDocument.LoadXml(xml);

            var returnItemsList = new List<T>();

            foreach (XmlNode xmlNode in xmlDocument.SelectNodes(nodePath))
            {
                returnItemsList.Add(XmlToObject<T>(xmlNode.OuterXml));
            }

            return returnItemsList;
        }

        public static void EndProg()
        {
            //Console.WriteLine("Program complete.");
            //Console.WriteLine("Strike ENTER to terminate.");
            //Console.ReadLine();
        }

        public static void ExecuteCommandSync(string command)
        {
            try
            {
                // create the ProcessStartInfo using "cmd" as the program to be run,
                // and "/c " as the parameters.
                // Incidentally, /c tells cmd that we want it to execute the command that follows,
                // and then exit.
                System.Diagnostics.ProcessStartInfo procStartInfo =
                    new System.Diagnostics.ProcessStartInfo("cmd", "/c " + command);
                    //new System.Diagnostics.ProcessStartInfo(command, "");

                // The following commands are needed to redirect the standard output.
                // This means that it will be redirected to the Process.StandardOutput StreamReader.
                procStartInfo.RedirectStandardOutput = true;
                procStartInfo.UseShellExecute = false;
                // Do not create the black window.
                procStartInfo.CreateNoWindow = true;
                // Now we create a process, assign its ProcessStartInfo and start it
                System.Diagnostics.Process proc = new System.Diagnostics.Process();
                proc.StartInfo = procStartInfo;
                proc.Start();
                // Get the output into a string
                string result = proc.StandardOutput.ReadToEnd();
                // Display the command output.
                Console.WriteLine(result);
            }
            catch (Exception objException)
            {
                // Log the exception
                Console.WriteLine("Exception.");
                Console.WriteLine(objException);
                Console.ReadLine();
            }
        }

        public static string readFile(string filename)
        {
            StreamReader reader = new StreamReader(filename);
            string contents = reader.ReadToEnd();
            reader.Close();
            return contents;
        }

        public static void deleteFile(string filename)
        {
            FileInfo fi = new FileInfo(filename);
            fi.Delete();
        }

        public static string GatherJobs() {
            string jobs = "";
            //string cmd = "\"C:\\Program Files (x86)\\Quest Software\\NetVault Backup\\util\\nvjoblist.exe\" -server hovh01 -runinfo -delimiter * > rawjobs.txt";
            string cmd = "\"C:\\Program Files (x86)\\Quest Software\\NetVault Backup\\util\\nvjoblist.exe\" -runinfo -delimiter * > rawjobs.txt";
            //string cmd = "echo hello > rawjobs.txt";
            ExecuteCommandSync(cmd);
            jobs = readFile("rawjobs.txt");
            deleteFile("rawjobs.txt");
            return jobs;
        }

        public static List<NVBackupJob> ReleventJobs(List<NVBackupJob> Jobs)
        {
            List<NVBackupJob> Results = new List<NVBackupJob>();

            foreach (NVBackupJob SingleJob in Jobs)
            {
                if (SingleJob.RunStatus == "Scheduled" || SingleJob.RunStatus == "Running")
                    Results.Add(SingleJob);
            }

            return Results;
        }

        public static List<NVBackupLog> GetJobLogs(List<NVBackupJob> Jobs)
        {
            List<NVBackupLog> LogList = new List<NVBackupLog>();

            foreach (NVBackupJob Job in Jobs)
            {
                Console.WriteLine("Getting logs for Job ID " + Job.ID);
                //string TempFilename = "\\\\hovh01\\c$\\sp_test\\temp_logs_" + Job.ID + ".tmp";
                string TempFilename = "C:\\Program Files (x86)\\Quest Software\\NetVault Backup\\logs\\dumps\\text\\temp_logs_" + Job.ID + ".tmp";
                //string cmd = "\\SysinternalsSuite\\PsExec.exe \\\\hovh01 \"C:\\Program Files (x86)\\Quest Software\\NetVault Backup\\util\\nvlogdump.exe\" -jobid " + Job.ID + " -filename \"" + TempFilename + "\" -text";
                string cmd = "\"C:\\Program Files (x86)\\Quest Software\\NetVault Backup\\util\\nvlogdump.exe\" -jobid " + Job.ID + " -filename " + "temp_logs_" + Job.ID + ".tmp" + " -text";
                Console.WriteLine("Executing: " + cmd);
                //Console.WriteLine(cmd);
                ExecuteCommandSync(cmd);
                string RawLog = readFile(TempFilename);
                //Console.WriteLine(RawLog);
                NVBackupLog Log = new NVBackupLog();
                Log.ID = Job.ID;
                //Log.Log = RawLog;
                deleteFile(TempFilename);
                //Console.WriteLine(Log.Log.Length + " characters dumped.");
                List<string> l = RawLog.Split('\n').ToList<string>();

                Int32 index = -1;
                for (Int32 i = 0; i < l.Count; i++)
                {
                    if (l[i].Contains("Starting phase 1 on"))
                        index = i;
                }

                if (index != -1)
                {
                    for (Int32 i = index; i < l.Count; i++)
                        Log.Log = Log.Log + l[i] + "\n";
                }

                //Console.WriteLine(l[l.Count - 2]);
                Console.WriteLine(Log.Log.Split('\n').ToList<string>().Count + " lines recorded.");
                LogList.Add(Log);
            }

            return LogList;
        }

        public static int BackupJobIndex(List<NVBackupJob> Jobs, string Target)
        {
            for (Int32 i = 0; i < Jobs.Count; i++)
                if (Jobs[i].ID == Target)
                    return i;
            return -1;
        }

        public static Int32 BackupLogIndex(List<NVBackupLog> Logs, string Target)
        {
            for (Int32 i = 0; i < Logs.Count; i++)
                if (Logs[i].ID == Target)
                    return i;
            return -1;
        }

        public static string Delimit(string info, string delim)
        {
            string result = "";
            int b = info.IndexOf(delim) + delim.Length;
            int e = info.IndexOf("/" + delim);
            result = info.Substring(b + 1, e - b - 2);
            return result;
        }

        static void Main(string[] args)
        {
            string JobsRaw = GatherJobs();
            Console.WriteLine(JobsRaw);

            //NVBackupJob job1 = new NVBackupJob();
            List<NVBackupJob> Jobs = new List<NVBackupJob>();
            List<string> AllJobsRaw = JobsRaw.Split('\n').ToList<string>();
            Console.WriteLine(AllJobsRaw.Count);
            for (Int32 i = 1; i < AllJobsRaw.Count; i++)
            {
                if (AllJobsRaw[i].Length > 0)
                {
                    NVBackupJob TempJob = new NVBackupJob();
                    TempJob.ReadLine(AllJobsRaw[i], '*');
                    //Console.WriteLine(AllJobsRaw[i]);
                    Jobs.Add(TempJob);
                }
            }
            
            foreach (NVBackupJob Job in Jobs)
            {
                Console.WriteLine(Job.ToString());
            }

            Console.WriteLine("\n\n\n");

            foreach (NVBackupJob Job in ReleventJobs(Jobs))
            {
                Console.WriteLine(Job.ToString());
            }
            Console.WriteLine(ReleventJobs(Jobs).Count + " active jobs.");
            Console.WriteLine("\n\n\n");

            List<NVBackupLog> JobLogs = GetJobLogs(ReleventJobs(Jobs));

            foreach (NVBackupLog Log in JobLogs)
            {
                Console.WriteLine(Log.ID);
                Console.WriteLine(Log.Log.Split('\n').ToList<string>()[0]);
                Console.WriteLine(Log.Log.Split('\n').ToList<string>()[Log.Log.Split('\n').ToList<string>().Count - 3]);
            }

            for (Int32 i = 0; i < Jobs.Count; i++)
            {
                int index = BackupLogIndex(JobLogs, Jobs[i].ID);
                if (index != -1)
                {
                    NVBackupLine line = new NVBackupLine(Jobs[i], JobLogs[index]);
                    //Console.WriteLine(line);
                    //Console.WriteLine(line.generateXml());
                    //Console.WriteLine();
                    //NVBackupLine a = new XmlToObject<NVBackupLine>(line.generateXml());
                    //NVBackupLine a = (NVBackupLine)CreateObject(line.generateXml(), a);

                    /*string xml = line.generateXml();

                    NVBackupLine j = new NVBackupLine();
                    j.ID = Delimit(xml, "ID");
                    j.Client = Delimit(xml, "Client");
                    j.Title = Delimit(xml, "Title");
                    j.Status = Delimit(xml, "Status");
                    j.Timestamp = Convert.ToDateTime(Delimit(xml, "Timestamp"));
                    j.Log = Delimit(xml, "Log");

                    Console.WriteLine(j.ToString());*/
                    Console.WriteLine(NVBackupLine.readXml(line.generateXml()).ToString());
                    

                    Console.WriteLine("Adding to database.");

                    string url = "http://hosrv.za.illovo.net/NVDataInterface/NVBackupInterface.aspx";
                    //string url = "http://localhost:27886/NVBackupInterface.aspx";
                    WebClient wb = new WebClient();
                    NameValueCollection data = new NameValueCollection();
                    data["payload"] = line.generateXml();
                    try
                    {
                        byte[] response = wb.UploadValues(url, "POST", data);
                        Console.WriteLine(Encoding.ASCII.GetString(response));
                    }
                    catch (Exception e)
                    {
                        Console.Write(e.ToString());
                    }


                    //XElement reader = XElement.Parse(xml, LoadOptions.None);

                    /*IEnumerable<XAttribute> attList = reader.Attributes("ID");
                    foreach (XAttribute att in attList)
                        Console.WriteLine(att);
                    Console.WriteLine(reader.ToString());
                    Console.ReadLine();*/
                    /*var channelLst=doc.Elements("channel").Select(x=>
                        new
                        {
                            id=x.Attribute("id").Value,
                            displayName=x.Element("display-name").Value,
                            icon=x.Element("icon").Attribute("src").Value
                        }*/
                    /*var info = reader.Elements("NVBackupLine").Select(x =>
                        new
                        {
                            ID = x.Attribute("ID").Value
                        });
                    Console.WriteLine(info.ToString());*/
                    
                }
                
            }



            EndProg();
        }
    }
}
